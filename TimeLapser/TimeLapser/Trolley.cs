﻿using System;
using System.IO.Ports;
using System.Collections.Generic;

namespace TimeLapse.SerialCommunication {

	public class Trolley {

		private const string TRAME_READY = "Ready";
		private const string TRAME_END = "End";

		private const string REPLY_OK = "Ok";
		private const string REPLY_ERROR = "Error";

		private SerialPort serialPort;

		// Constructeur
		public Trolley() {
		}

		public string[] getPortsList(){
		
			return SerialPort.GetPortNames ();
		
		}

		// Envoie d'une Trame
		public void sendTrame(string str) {

			Trame trame = new Trame();
			string requete = trame.buildTrame(str);
			serialPort.WriteLine(requete);

		}

		// initialise le Trolley
		public void initialize(string ports) {

			// Ouverture de la liaison série
			// On donne juste le nom du port les autres paramètres de la
			// connexion restent ceux par défaut.
			this.serialPort = new SerialPort();
			this.serialPort.PortName = ports;
			this.serialPort.Open ();

			// On vérifie si le port est correctement ouvert.
			if(!this.serialPort.IsOpen){
				// écriture de log dans la console.
			}
			else{
				// écriture de log dans la console.
			}

			// Envoie d'une trame au trolley pour savoir si ce dernier est prêt.
			this.sendTrame(TRAME_READY);

		}

		// Bouger le Trolley au coordonnées voulues
		public void move(float pos, float angle) {
		}

		// Vérifie la réponse du Trolley
		public bool checkReply() {

			// On lit la réponse.
			string replyValue = this.serialPort.ReadLine();

			// On vérifie la réponse pour savoir si la lecture c'est bien passé.
			if(replyValue == REPLY_OK) {

				return true;

			}
			else {

				return false;

			}

		}

		public void unInitialize() {

			this.serialPort.Close();

		}

	}
}
