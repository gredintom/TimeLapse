﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeLapse.SerialCommunication {
	public class Trame {

		private const string START_CHARACTER = "$";
		private const string END_CHARACTER = "!";
		private const string SEPARATE_CHARACTER = ";";

		string data;

		// Constructeur
		public Trame() {

		}

		// Construit une Trame
		public string buildTrame(string data) {

			// mettre en place le format des données.
			string trame = START_CHARACTER + data + SEPARATE_CHARACTER;

			// Calcul du checksum.
			int checksum = calculateChecksum(trame);

			// Fin de la construction de la trame.
			trame += checksum + SEPARATE_CHARACTER + END_CHARACTER;

			Console.WriteLine(trame);

			return trame;

		}

		// Calucul le checksum de la Trame
		private int calculateChecksum(string trame) {

			int checksum = 0;

			// On récupère un tableau contenant les valeurs ASCII des 
			// caractères présents dans la trame.
			byte[] values = Encoding.ASCII.GetBytes(trame);

			// Maintenant pour chaque valeurs on fait la somme pour obtenir la
			// somme de controle.
			foreach(byte value in values){

				checksum += (int)value;
				
			}

			return checksum;

		}

	}
}
