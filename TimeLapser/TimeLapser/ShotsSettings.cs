﻿namespace TimeLapse {

	public struct ShotsSettings {

		/*
			Nom de la prise de vue.
		*/
		public string name {
			get; set;
		}

		/*
			Description de la prise de vue.
		*/
		public string description {
			get; set;
		}

		/*
			Nom du port série utilisé pour la prise de vue.
		*/
		public string portName {
			get; set;
		}

		/*
			Chemin vers le dossier du projet.
		*/
		public string projectDirectory {
			get; set;
		}

	}

}
