﻿namespace TimeLapser {
	partial class IHM {
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent() {
			this.label1 = new System.Windows.Forms.Label();
			this.ShotNameEntry = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.portsDropDownList = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ShotDescriptEntry = new System.Windows.Forms.RichTextBox();
			this.sendButton = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.pathEntry = new System.Windows.Forms.TextBox();
			this.findDirectory_Button = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(116, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nom de la prise de vue";
			// 
			// ShotNameEntry
			// 
			this.ShotNameEntry.Location = new System.Drawing.Point(15, 26);
			this.ShotNameEntry.Name = "ShotNameEntry";
			this.ShotNameEntry.Size = new System.Drawing.Size(244, 20);
			this.ShotNameEntry.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Sélection du port série";
			// 
			// portsDropDownList
			// 
			this.portsDropDownList.FormattingEnabled = true;
			this.portsDropDownList.Location = new System.Drawing.Point(15, 66);
			this.portsDropDownList.Name = "portsDropDownList";
			this.portsDropDownList.Size = new System.Drawing.Size(244, 21);
			this.portsDropDownList.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(262, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(162, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Commentaires sur la prise de vue";
			// 
			// ShotDescriptEntry
			// 
			this.ShotDescriptEntry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ShotDescriptEntry.Location = new System.Drawing.Point(265, 26);
			this.ShotDescriptEntry.Name = "ShotDescriptEntry";
			this.ShotDescriptEntry.Size = new System.Drawing.Size(402, 96);
			this.ShotDescriptEntry.TabIndex = 5;
			this.ShotDescriptEntry.Text = "";
			// 
			// sendButton
			// 
			this.sendButton.Location = new System.Drawing.Point(13, 134);
			this.sendButton.Name = "sendButton";
			this.sendButton.Size = new System.Drawing.Size(115, 23);
			this.sendButton.TabIndex = 6;
			this.sendButton.Text = "Envoyer";
			this.sendButton.UseVisualStyleBackColor = true;
			this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(15, 94);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(165, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "chemin d\'enregistrement du projet";
			// 
			// pathEntry
			// 
			this.pathEntry.Location = new System.Drawing.Point(15, 108);
			this.pathEntry.Name = "pathEntry";
			this.pathEntry.Size = new System.Drawing.Size(202, 20);
			this.pathEntry.TabIndex = 8;
			// 
			// findDirectory_Button
			// 
			this.findDirectory_Button.Location = new System.Drawing.Point(223, 108);
			this.findDirectory_Button.Name = "findDirectory_Button";
			this.findDirectory_Button.Size = new System.Drawing.Size(35, 20);
			this.findDirectory_Button.TabIndex = 9;
			this.findDirectory_Button.Text = "...";
			this.findDirectory_Button.UseVisualStyleBackColor = true;
			this.findDirectory_Button.Click += new System.EventHandler(this.findDirectory_Button_Click);
			// 
			// IHM
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(679, 420);
			this.Controls.Add(this.findDirectory_Button);
			this.Controls.Add(this.pathEntry);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.sendButton);
			this.Controls.Add(this.ShotDescriptEntry);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.portsDropDownList);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.ShotNameEntry);
			this.Controls.Add(this.label1);
			this.Name = "IHM";
			this.Text = "Time Lapser";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox ShotNameEntry;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox portsDropDownList;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RichTextBox ShotDescriptEntry;
		private System.Windows.Forms.Button sendButton;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox pathEntry;
		private System.Windows.Forms.Button findDirectory_Button;
	}
}

