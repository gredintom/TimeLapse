﻿using System;
using TimeLapse.SerialCommunication;
using System.Collections.Generic;
using System.IO;

namespace TimeLapse
{
	public class App{

		// Constante pour le nom des différents sous répertoire d'un projet.
		private const string PICTURES_DIRECTORY = "Pictures";
		private const string RESIZED_PICTURES_DIRECTORY = "Resized_Pictures";
		private const string VIDEO_DIRECTORY = "Video";

		private Trolley trolley;
		
		public App (){

			trolley = new Trolley();

		}

		public string[] getPortsList(){
		
			return trolley.getPortsList ();
		
		}

		public void sendSettings(ShotsSettings settings){

			calculateMovements();
			prepareProjectDirectory(settings);
			//trolley.initialize(settings.portName);

		}

		private void calculateMovements(){
			
		}

		/*
			Permet de préparer le doossier au seront enregistré les différents éléments du
			projet qui composent la prise de vue.
		*/
		private void prepareProjectDirectory(ShotsSettings settings) {

			string filePath = settings.projectDirectory + "/" + settings.name + "/" + settings.name + ".txt";
			Directory.CreateDirectory(settings.projectDirectory + "/" + settings.name);
			Directory.CreateDirectory(settings.projectDirectory + "/" + settings.name + "/" + PICTURES_DIRECTORY);
			Directory.CreateDirectory(settings.projectDirectory + "/" + settings.name + "/" + RESIZED_PICTURES_DIRECTORY);
			Directory.CreateDirectory(settings.projectDirectory + "/" + settings.name + "/" + VIDEO_DIRECTORY);

			StreamWriter writer = new StreamWriter(filePath, true);
			writer.WriteLine("Name : " + settings.name + "\n");
			writer.WriteLine("Description : " + settings.description);
			writer.Flush();
			writer.Close();

		}

	}
}

