﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Include des classes du projet.
using TimeLapse;

namespace TimeLapser {

	public partial class IHM : Form {

		App application;

		public IHM() {

			this.application = new App();
			InitializeComponent();

			// Récupération de la liste des ports série présent sur la machine.
			// Et construction de la liste
			string[] portsList = getPortsList();
			buildDropDownList(portsList);

		}

		/*
			Permet de récupérer la liste des ports série présents sur la
			machine.
		*/
		private string[] getPortsList() {

			return this.application.getPortsList();

		}

		/*
			Permet de construire la liste déroulante qui expose les ports séries.
		*/
		private void buildDropDownList(string[] portsList) {

			foreach(var port in portsList) {

				portsDropDownList.Items.Add(port);

			}

		}

		/*
			Permet de vérifier si les paramètres données par l'utilisateur
			sont correct pour une prise de vue.
		*/
		private bool checkSettings(ShotsSettings settings) {

			return true;

		}

		/*
			Quand l'utilisateur clique sur le bouton envoyer.
		*/
		private void sendButton_Click(object sender, EventArgs e) {

			// Récupération des infos.
			ShotsSettings settings = new ShotsSettings();
			settings.name = ShotNameEntry.Text;
			settings.description = ShotDescriptEntry.Text;
			settings.portName = portsDropDownList.SelectedText;
			settings.projectDirectory = pathEntry.Text;

			if(checkSettings(settings)) {

				this.application.sendSettings(settings);

			}
			else {

				/* Message prévennant que les réglages ne sont pas bons. */
				MessageBox.Show("Les paramètres ne sont pas valides.");

			}

		}

		/*
			Permet de faire la recherche d'un répertoire ou sera sauvegardé le projet.
			Le résultat de cette recherche est stoqué dans la ligne de texte à gauche du
			bouton.
		*/
		private void findDirectory_Button_Click(object sender, EventArgs e) {

			FolderBrowserDialog dialog = new FolderBrowserDialog();
			dialog.Description = "Sélection du dossier ou sera enregistré le projet.";
			dialog.ShowNewFolderButton = true;

			dialog.ShowDialog();

			pathEntry.Text = dialog.SelectedPath;

		}
	}
}
