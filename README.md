<h1>Projet : TimeLapse</h1>

<b>Auteurs</b> : 
+ Andrew Cooper
+ Camille Jeantot
+ Thomas Gredin

<h2>Description</h2>
Ce projet à pour but, la création d'un soft permettant le contrôle d'un chariot
qui réalise un travelling sur un rail ainsi qu'un appareil photo de la marque
Canon pour réaliser des montages de type time lapse.

Pour cela le projet se base sur le SDK proposé par Canon pour ce qui concerne le
contôle de l'appareil photo.

Le système possède deux modes de fonctionnement qui sont les suivants :
+ Le mode autonome 
+ Le mode automatisé

<h5>Mode Autonome : </h5>
Ce mode permet de ne pas laisser l'ordinateur branché au chariot et à l'appareil
photo tout le long de la prise de vue qui peut duré un certain temps. Ainsi on se
retrouve avec un appareil photo qui est contrôlé par le chariot à l'aide de petites
impulsions électriques qui correspondent à des actions pour l'appareil photo.
A la fin de la prise de vue il suffit de rebrancher l'ordinateur à l'appareil photo
pour que ce dernier puisse récupérer les photos et les traiter avant de créer une
vidéo en sortie qui sera le time lapse final correspondant aux réglages données par
l'utilisateur lors de la configuration du projet.

<h5>Mode Automatisé : </h5>
Ce mode est semblable dans son fonctionnement au mode autonome sauf que tout est
contrôlé par l'ordinateur. La fin du processus se passe de la même façon que pour
le mode autonome.

<h2>Updates</h2>
+ 29 Mars 2016 :
    - Création du dépot
    - Nouvelle fonctionnalité : création automatique d'un répertoire de projet
    pour chaque nouveau projet de prise de vue.